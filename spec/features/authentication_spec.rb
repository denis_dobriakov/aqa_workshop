feature 'Login with created user', js: true do
  scenario 'Register new user' do
    register_user
    sleep 2
  end

  scenario 'Visitor makes login' do
    login_user($user, $password)
    expect(page).to have_content $user
    expect(find('a.my-account')).to be_visible
    expect(find('a.logout')).to be_visible
  end

   scenario 'User successfully logs out' do
     visit '/login'
     login_user($user, $password)
     logout_user

     expect(page).not_to have_content $user
     expect(page.current_url).to include 'http://demo.redmine.org/'
   end

end
