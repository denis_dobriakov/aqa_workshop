

 feature 'Visitor visit homepage', js: true do

  scenario 'Visitor successfully visits homepage' do
    sleep 1
    visit '/'
    sleep 1
    expect(page.current_url).to include 'http://demo.redmine.org/'
    expect(page).to have_content 'Redmine demo'
  end

  scenario 'Visitor successfully navigates to login page' do
    visit '/'
    sleep 1
    find(:xpath, '//*[@id="account"]/ul/li[1]/a').click
    sleep 1
    expect(page.current_url).to include '/login'
    expect(find_field('username')).to be_visible
    expect(find_field('password')).to be_visible
    expect(find(:xpath, '//*[@id="login-form"]/form/table/tbody/tr[4]/td[2]/input')).to be_visible

  end
end
