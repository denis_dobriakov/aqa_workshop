module FeatureHelper



  def logout_user
    find('a.logout').click
  end

  def register_user
    $user = SecureRandom.hex(4)
    $password = SecureRandom.base64(5)
    @first_name = SecureRandom.base64(5)
    @last_name = SecureRandom.base64(5)
    @email = (SecureRandom.hex(2) + '@' + SecureRandom.hex(2) + '.com')
    visit '/login'
    find('a.register').click
    find_field('user_login').set $user
    find_field('user_password').set $password
    find_field('user_password_confirmation').set $password
    find_field('user_firstname').set @first_name
    find_field('user_lastname').set @last_name
    find_field('user_mail').set @email
    find_button('Submit').click
   end

   def login_user(user, password)
     visit '/login'
     click_on 'Sign in'
     find_field('Login:').set user
     find_field('Password').set password
     click_on 'Login »'
   end

   def projects_page
     find('a.projects').click
   end

end
